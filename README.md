i++
===

Mojang has brilliant artists. But I can’t help feeling that Minecraft, by
default, goes a little overboard on the local contrast. So I’ve made a resource
pack that tones that down a bit, alongside a few other small improvements.

It’s cozy Minecraft!

Note: the fonts in this texture pack are copied from
[Huahwi’s default edit](https://youtu.be/wIqzCkldzlE).

installing
----------

Simply download this project as a zip archive or clone the repository. It should
probably go into ~/.minecraft/resourcepacks, and you can rename it to something
like `§8imyxh§7 - §fi++ §7[16x]` if you like pretty formatting in the pack
selection menu.

